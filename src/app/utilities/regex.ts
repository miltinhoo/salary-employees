export function validateIdRegex(id: string): boolean {
    const regex: RegExp = new RegExp(/^[0-9]*$/);
    return id && String(id).match(regex) ? true : false;
}