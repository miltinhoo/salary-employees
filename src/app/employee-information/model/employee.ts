export interface Employee {
    id: number;
    name: string;
    contractType: string;
    roleName: string;
    roleDescription: string;
    annualSalary: number
}

export class ErrorModel {
    errorMessage: string;
    constructor(errorMessage: string) {
        this.errorMessage = errorMessage;
    }
}

export const employeesMock: Employee[] = [
    {
        id: 1,
        name: 'Goky',
        contractType: 'HourlySalaryEmployee',
        roleName: 'Administrator',
        roleDescription: undefined,
        annualSalary: 1000000
    }
]
