import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { EmployeeDataService } from '../services/employee-data/employee-data.service';
import { tap } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { Employee } from '../model/employee';

@Component({
  selector: 'app-employee-info',
  templateUrl: './employee-info.component.html',
  styleUrls: ['./employee-info.component.scss']
})
export class EmployeeInfoComponent implements OnInit, OnDestroy {

  employeeData: Employee[];
  isLoading: boolean;

  employeeData$ = new Subscription();
  isLoading$ = new Subscription();

  constructor(
    private employeeDataService: EmployeeDataService
  ) { }

  ngOnInit() {
    this.getEmployeeData();
    this.getIsLoading();
  }

  ngOnDestroy() {
    this.employeeData$.unsubscribe();
    this.isLoading$.unsubscribe();
  }

  getEmployeeData(): void {
    this.employeeData$ = this.employeeDataService.currentEmployeeInfo.pipe(
      tap((employeeData: Employee[]) => this.employeeData = employeeData)
    ).subscribe();
  }

  getIsLoading(): void {
    this.isLoading$ = this.employeeDataService.currentIsLoading.pipe(
      tap((isLoading: boolean) => this.isLoading = isLoading)
    ).subscribe();
  }

}
