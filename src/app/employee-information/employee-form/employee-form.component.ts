import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { catchError, finalize, first, tap } from 'rxjs/operators';
import { Employee, ErrorModel } from '../model/employee';
import { EmployeeDataService } from '../services/employee-data/employee-data.service';
import { EmployeeInfoService } from '../services/employee-info/employee.service';
import { EmployeeUtilsService } from '../services/utils/employee-utils.service';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.scss']
})
export class EmployeeFormComponent implements OnInit {

  form: FormGroup;
  error: ErrorModel;

  constructor(
    private fb: FormBuilder,
    private employeeService: EmployeeInfoService,
    private employeeDataService: EmployeeDataService,
    private employeeUtilsService: EmployeeUtilsService
    ) { }

    ngOnInit() {
      this.createForm();
    }

  createForm(): void {
    this.form = this.fb.group({
      employeeId: ['', [this.employeeUtilsService.validateId()]]
    });
  }

  getEmployee(): void {
    if (this.form.valid) {
      const employeeId: string = this.form.controls.employeeId ? this.form.controls.employeeId.value : undefined;
      const employeeData = employeeId ? this.employeeService.getEmployeeInfoById(employeeId) : this.employeeService.getAllEmployeeInfo();
      this.goToService(employeeData);
    } else {
      this.employeeDataService.changeIsLoading(false);
    }
  }

  goToService(employeeData: Observable<any>): void {
    employeeData.pipe(
      tap((employeeData: Employee[] | ErrorModel) => {
        if (employeeData instanceof ErrorModel) {
          this.error = employeeData;
        } else {
          this.employeeDataService.changeEmployeeInfoValue(employeeData);
        }
      }),
      catchError((error: ErrorModel) => of(this.error = error)),
      finalize(() => this.employeeDataService.changeIsLoading(false)),
      first()
    ).subscribe();
  }

  initValues():void {
    this.employeeDataService.changeEmployeeInfoValue(undefined);
    this.employeeDataService.changeIsLoading(true);
    this.error = undefined;
  }

}
