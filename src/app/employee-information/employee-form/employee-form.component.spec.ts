import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EmployeeDataService } from '../services/employee-data/employee-data.service';
import { EmployeeInfoService, EmployeeInfoServiceMock } from '../services/employee-info/employee.service';
import { EmployeeUtilsService } from '../services/utils/employee-utils.service';
import { EmployeeFormComponent } from './employee-form.component';
import { of } from 'rxjs';
import { employeesMock } from '../model/employee';


describe('EmployeeFormComponent', () => {
  let component: EmployeeFormComponent;
  let fixture: ComponentFixture<EmployeeFormComponent>;

  const employeeInfoServiceMock = new EmployeeInfoServiceMock();
  let employeeDataService: EmployeeDataService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EmployeeFormComponent],
      providers: [
        { provide: EmployeeInfoService, useValue: employeeInfoServiceMock },
        EmployeeDataService,
        EmployeeUtilsService
      ],
      imports: [
        ReactiveFormsModule,
        FormsModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    employeeDataService = fixture.debugElement.injector.get(EmployeeDataService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#formButton', () => {
    it(`If we press the form button, two methods must be called,
          the first to initialize values and the second to invoke the service`, () => {
      // Arrange
      component.form.controls.employeeId.setValue(1234);
      spyOn(component, 'initValues');
      const getEmployeeSpy = spyOn(component, 'getEmployee');

      // Action
      fixture.detectChanges();
      const btn: HTMLButtonElement = fixture.debugElement.nativeElement.querySelector('#btn-form');
      btn.click();
      fixture.detectChanges();

      // Assert
      expect(component.initValues).toHaveBeenCalledTimes(1);
      expect(component.initValues).toHaveBeenCalledBefore(getEmployeeSpy);
      expect(component.getEmployee).toHaveBeenCalledTimes(1);

    });
  });

  describe('#getEmployee', () => {
    it(`getEmployee method validates the form,
          in this case the form is valid and the method calls other methods for get employee information by id`, () => {
      // Arrange
      const idValue = 1234;
      const getEmployeeInfoByIdMockVal = of(employeesMock);
      component.form.controls.employeeId.setValue(idValue);

      spyOn(employeeInfoServiceMock, 'getEmployeeInfoById').and.returnValue(getEmployeeInfoByIdMockVal);
      spyOn(employeeInfoServiceMock, 'getAllEmployeeInfo');
      spyOn(component, 'goToService');
      spyOn(employeeDataService, 'changeIsLoading');

      // Action
      component.getEmployee();

      // Assert
      expect(employeeInfoServiceMock.getEmployeeInfoById).toHaveBeenCalledWith(idValue);
      expect(employeeInfoServiceMock.getAllEmployeeInfo).not.toHaveBeenCalled();
      expect(component.goToService).toHaveBeenCalledWith(getEmployeeInfoByIdMockVal);
      expect(employeeDataService.changeIsLoading).not.toHaveBeenCalled();
    });

    it(`getEmployee method validates the form,
        In this case the form is valid and the method calls other methods to obtain the information of all employees`, () => {
      // Arrange
      const getAllEmployeeInfoMockVal = of(employeesMock);
      component.form.controls.employeeId.setValue(undefined);

      spyOn(employeeInfoServiceMock, 'getEmployeeInfoById');
      spyOn(employeeInfoServiceMock, 'getAllEmployeeInfo').and.returnValue(getAllEmployeeInfoMockVal);
      spyOn(component, 'goToService');
      spyOn(employeeDataService, 'changeIsLoading');

      // Action
      component.getEmployee();

      // Assert
      expect(employeeInfoServiceMock.getEmployeeInfoById).not.toHaveBeenCalled();
      expect(employeeInfoServiceMock.getAllEmployeeInfo).toHaveBeenCalled();
      expect(component.goToService).toHaveBeenCalledWith(getAllEmployeeInfoMockVal);
      expect(employeeDataService.changeIsLoading).not.toHaveBeenCalled();
    });

    it(`getEmployee method validates the form,
        In this case the form isn't valid and the method calls only one method to stop the spinner`, () => {
      // Arrange
      const getAllEmployeeInfoMockVal = of(employeesMock);
      component.form.controls.employeeId.setValue('97A');

      spyOn(employeeInfoServiceMock, 'getEmployeeInfoById');
      spyOn(employeeInfoServiceMock, 'getAllEmployeeInfo');
      spyOn(component, 'goToService');
      spyOn(employeeDataService, 'changeIsLoading');

      // Action
      component.getEmployee();

      // Assert
      expect(employeeInfoServiceMock.getEmployeeInfoById).not.toHaveBeenCalled();
      expect(employeeInfoServiceMock.getAllEmployeeInfo).not.toHaveBeenCalled();
      expect(component.goToService).not.toHaveBeenCalled();
      expect(employeeDataService.changeIsLoading).toHaveBeenCalledWith(false);
    });
  });
});
