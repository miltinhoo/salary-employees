import { TestBed } from '@angular/core/testing';

import { EmployeeInfoService } from './employee.service';
import { HttpClientModule } from '@angular/common/http';

describe('EmployeeService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule
    ]
  }));

  it('should be created', () => {
    const service: EmployeeInfoService = TestBed.get(EmployeeInfoService);
    expect(service).toBeTruthy();
  });
});
