import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, map, catchError } from 'rxjs/operators';
import { Employee, ErrorModel } from '../../model/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeInfoService {

  constructor(private http: HttpClient) { }

  getAllEmployeeInfo(): Observable<Employee[] | ErrorModel> {
    return this.http.get('http://localhost:8080/employees/').pipe(
      map((response: any) => !response.errorMessage ? this.respToEmployees(response) : new ErrorModel(response.errorMessage))
    );
  }

  getEmployeeInfoById(employeeId: string): Observable<Employee[] | ErrorModel> {
    return this.http.get(`http://localhost:8080/employees/${employeeId}`).pipe(
      map((response: any) => !response.errorMessage ? this.respToEmployee(response) : new ErrorModel(response.errorMessage)),
      catchError(this.handleError)
    );
  }

  respToEmployee(response: any): Employee[] | ErrorModel {
    return response.employee ? [{
      id: response.employee.id,
      name: response.employee.name,
      contractType: response.employee.contractType,
      roleName: response.employee.roleName,
      roleDescription: response.employee.roleDescription,
      annualSalary: response.employee.annualSalary
    }] : undefined;
  }

  respToEmployees(response: any): Employee[] | ErrorModel {
    return response.employees ? response.employees.map((resp: any) => ({
      id: resp.id,
      name: resp.name,
      contractType: resp.contractType,
      roleName: resp.roleName,
      roleDescription: resp.roleDescription,
      annualSalary: resp.annualSalary
    })) : undefined;
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(new ErrorModel('Something bad happened; please try again later.'));
  };
}

export class EmployeeInfoServiceMock {
  getAllEmployeeInfo(): Observable<Employee[] | ErrorModel> {
    return of(undefined);
  }

  getEmployeeInfoById(employeeId: string): Observable<Employee[] | ErrorModel> {
    return of(undefined);
  }
}
