import { TestBed } from '@angular/core/testing';

import { EmployeeUtilsService } from './employee-utils.service';

describe('EmployeeUtilsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmployeeUtilsService = TestBed.get(EmployeeUtilsService);
    expect(service).toBeTruthy();
  });
});
