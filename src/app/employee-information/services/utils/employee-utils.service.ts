import { Injectable } from '@angular/core';
import { ValidatorFn, AbstractControl } from '@angular/forms';
import { validateIdRegex } from '../../../utilities/regex';

@Injectable({
  providedIn: 'root'
})
export class EmployeeUtilsService {

  constructor() { }

  validateId(): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const id: string = control.value;
      if (id && !validateIdRegex(id)) {
        return { invalidData: true };
    }
      return undefined;
    };
  }
}
