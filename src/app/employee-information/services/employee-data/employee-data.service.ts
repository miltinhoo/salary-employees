import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Employee } from '../../model/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeDataService {

  private employeeInfo = new BehaviorSubject<Employee[]>(undefined);
  currentEmployeeInfo = this.employeeInfo.asObservable();

  private isLoading = new BehaviorSubject<boolean>(false);
  currentIsLoading = this.isLoading.asObservable();

  constructor() { }
  
  changeEmployeeInfoValue(employeeInfo: Employee[]): void {
    this.employeeInfo.next(employeeInfo);
  }

  changeIsLoading(isLoading: boolean): void {
    this.isLoading.next(isLoading);
  }
}
